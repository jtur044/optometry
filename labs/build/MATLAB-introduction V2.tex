%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 File: OSAstyle.tex                  %
%                Date: Feb. 14, 2007                  %
%                                                     %
%    LaTeX template file for use with OSA journals    %
%         JOSA A, JOSA B, Applied Optics,             %
%            Optics Letters, and JON                  %
%                                                     %
%   This file requires the substyle file osajnl2.rtx, %
%       running under REVTeX 4.0 and LaTeX 2e,        %
%                           or                        %
%   the style file osajnl2.sty, running under LaTeX 2e%
%                                                     %
%       USE THE FOLLOWING REVTEX 4.0 OPTIONS:         %
%  \documentclass[osajnl2,preprint,showpacs]{revtex4} %
%                                                     %
%         USE THE FOLLOWING LaTeX OPTIONS:            %
%           \documentclass[12pt]{article}             %
%           \usepackage{osajnl2}                      %
%                                                     %
%                                                     %
%      (C) 2007 The Optical Society of America        %
%                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\documentclass[12pt,osajnl2,preprint,showpacs]{revtex4}  %% REVTeX 4.0

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Delete any REVTEX output files before running in LaTeX mode
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{article}   %% LaTeX 2e (preferred)
%\usepackage{osajnl2} %% do not use with REVTeX4
%\usepackage[draft]{hyperref} %% optional
%\usepackage{trackchanges}
%\usepackage{listings}
\usepackage{subfigure}
\usepackage{url}
\usepackage{graphicx}
\usepackage[framed, numbered, autolinebreaks, useliterate]{mcode}

\begin{document}
\title{A brief introduction to MATLAB}
\author{Jason Turuwhenua}
%\affiliation{Auckland Bioengineering Institute, \\ 
%Department of Optometry and Vision Science, \\
%University of Auckland}
%\email{j.turuwhenua@auckland.ac.nz}

\maketitle

\lstset{language=Matlab, basicstyle=\footnotesize,showstringspaces=false,} 

% abstract{Describe some properties of a defocuser comprised of two thin lenses with variable separation}

\tableofcontents
\pagebreak

%\section{Variables}
%\section{Manipulating variables}
%\section{Saving/loading variables}
%\section{Plottiing variables} 
%\section{Creating an image}
%\section{Loading/saving an image}
%\section{Convolution}
%\section{Putting commands together into a program}


\section{What is this document ? What is this \emph{not}?}

This is a guide to some very basic functionalities of the MATLAB programming environment.  
It is intended to assist you to ``get going'' with the environment, and to try out some examples related to images. 
It is not intended to be an in-depth tutorial of the MATLAB programming system.  

\section{What is MATLAB ?}

The MATLAB\textregistered  ~programming environment is two things: (1) a language and (2) an interactive environment in which commands (the language) can be executed. The environment allows the results of calculations to be managed  and visualized. The MATLAB programming environment is an extremely powerful tool that allows the user to \emph{test ideas quickly}. 

Whilst it is very flexible and comprehensive, we will be interested in using fairly basic functionalities that will allow us to investigate the content and manipulation of digital images. On the other hand, MATLAB is best learnt by doing, so {\bf don't be afraid to experiment with it!}.  There may be more than one way to achieve a given goal.

\section{The MATLAB programming environment}

\subsection{The startup screen}

Start the MATLAB program by navigating to the start icon, if you are using Windows.  On my system you will see something like what is shown in Figure \ref{fig:startup}.  The parts of the environment are briefly described.


\begin{figure}[h]
 \centering
  \includegraphics[scale=0.65]{./figures/figure-matlab-startup-annotated.png}
  \caption{MATLAB on startup}
  \label{fig:startup}
\end{figure}



\begin{description}
 \item[Workspace Window]  This window shows you what variables are currently loaded within the MATLAB environment . 
\item[Current Folder Window]  The current folder window shows you what the current folder on the computer MATLAB is. You will need to create a directory, and then enter it. This will be where your work is to be stored. 
\item[Command History] This window shows the commands that have been executed.
\item[Command Window]   This window is for entering your commands \emph{interactively}.  Interactive means that your commands will be executed as soon as you hit {\bf enter}, and the results are returned immediately.  Note that it is also possible to write a \emph{program}, that may be run from the command line.  This process (by contrast) is essentially non-interactive: the program steps through each line of the program file without interaction from the user.

\end{description}

\noindent Most of your time will be spent in the command window. This is where you enter commands that will instruct MATLAB what to do. It is customary to start a new language by writing a `Hello World'' program. In the command window type the following command

\bigskip

\begin{lstlisting}
disp('Hello World.');  % <- press enter 
\end{lstlisting}

\noindent The \lstinline!disp! command used to display text in the command window.  Type \lstinline!help disp!  (and press {\bf enter} to learn more about it.


\subsection{How to find help?}

Perhaps one of the most useful MATLAB commands is the help command.  An extensive list of help topics is available by typing:
\begin{lstlisting}[frame=single]
help
\end{lstlisting}

\noindent To find information on the many functions available that deal with images (for instance) type: 

\begin{lstlisting}[frame=single]
help images 
\end{lstlisting}

\noindent If you have a command you would like to know more about you can type \lstinline!help! followed by the name of the command. Alternately, you can navigate the help menus provided within the MATLAB environment. This may be useful if don't know the exact name of the command you are after.

\begin{figure}
 \centering
  \includegraphics[scale=0.35]{./figures/matlab-editor.png}
  \caption{MATLAB editor allows you to create a program.}
  \label{fig:matlab_editor}
\end{figure}

\subsection{Interactive and non-Interactive modes}

We have entered a few commands in the command window.  In doing so we have made use of MATLAB's  \emph{interactive mode}. In \emph{interactive} mode, you enter commands into the command window, and MATLAB executes the command immediately. 

\bigskip

There is another way to execute commands, and that is to invoke programs that will be executed by MATLAB essentially without user interaction. In this, \emph{non-interactive} mode, you run a \emph{program} (a pre-written set of instructions). To do this you will typically type the name of a MATLAB program file (it will have an extension of \lstinline!.m! and is referred to as an m-file).  When a MATLAB file is executed, MATLAB will execute the lines of the program by itself, until it completes (either successfully or with an error). 

\bigskip

\noindent It is noted that there are at least two other ways to run a program besides the method just mentioned. You may right click the m-file's name in the current folder window and then select \lstinline!run!, or alternately double click-it, so that it opens in the MATLAB editor, and then choose the \lstinline!run! option. Figure \ref{fig:matlab_editor} shows a program opened within the MATLAB editor.

\bigskip 

\subsection{Putting commands together into a program}

It is recommended that you add the commands you use in this lab into a \emph{program} or a script file (usually called an m-file). A program can be created by navigating to the {\bf File $\rightarrow$ New $\rightarrow$ Script } (accessed from MATLAB's navigation bar).  This  should open an empty file in the MATLAB editor for you to start entering commands in. Once this file is named e.g., \lstinline!helloworld.m! and saved, you will be able to run it.

\bigskip 

\noindent By entering the code you develop in this lab, to a file, you can  maintain a record of your work.   The results of the work can then be re-created by running the script/program usng one of the methods mentioned above.  This is a preferred method of working within MATLAB.

\bigskip

\noindent This tutorial document has an example program file called \lstinline!tutorial.m! associated with it, that can be downloaded into a directory on the computer. Create a directory (by selecting appropriate options in the current folder window), and place that file in there. Then enter the directory itself.  Now double click on this file. When the MATLAB editor opens you will see that it contains all the code for the examples in this tutorial.  You can run the code in this example non-interactively by typing \lstinline!tutorial! (in the command window).  Type:

\begin{lstlisting}[frame=single]
>>tutorial
\end{lstlisting}

\noindent It will run through each line of the file without you having to type in the commands yourself. This is convenient.  On the other hand, you may find it useful to copy relevant sections of code into the command window as you progress through the tutorial. Alternately, you may wish to forgo copying and pasting, and actually type in the code yourself.

\section{Elementary mathematical operations}

MATLAB can be used like a calculator. When using MATLAB interactively, you enter the calculation in full and then press {\bf enter} to execute the calculation. The result of the calculation will be returned. This results in ``code'' that looks similar to what you would write in ``real life'' math. 

The following examples demonstrate the most simple elementary operations \emph{i.e.}, addition, subtraction, multiplication. division.  You should try these out.

\begin{lstlisting}[frame=single]
>> 4+6
ans =
    10
>> 7-3
ans =
     4
>> 2*3
ans =
     6
>> 10/2
ans =
     5
\end{lstlisting}

\noindent Calculations can be arbitrarily complex. For example:

\begin{lstlisting}[frame=single]
>> 2+3+4-7*5+8/9+1-5*6/3 
ans =
  -34.1111
\end{lstlisting}

\noindent The standard rules of mathematics are obeyed.  We use parentheses to dis-ambiguate the order of operations 

\begin{lstlisting}[frame=single]
>> 2+3+4-7*(5+8/9)+1-5*6/3
ans =
  -41.2222
\end{lstlisting}

\noindent We can also use more ''advanced'' mathematical operators, including for example trigonometric functions, exponentials, powers and logarithms.  We will not look at these more advanced functions in this tutorial.


\subsection{Variables}

Variables are used to store numbers, and the results of calculations.  They can be single numbers (scalars) as we are more used to, or more complicated.  We will look at two additional types of variables: (1) vectors and (2) matrices. A vector is a list of numbers, whilst a matrix can be thought of as a grid (2D) or cube (3D)   of numbers. Higher dimensional objects are possible (for example, a color video is thought of as 4D) but we will not consider them here.

\bigskip

\noindent The simple example below creates two variables \lstinline!y! and \lstinline!x! and assigns to them the values $5$ and $12$ respectively

\begin{lstlisting}[frame=single]
>> y = 5; % <- press enter 
>> x = 12; % <- press enter 
\end{lstlisting}

\noindent These are simple numbers, so they are referred to as scalars. Experiment with addition, subtraction and scaling of these variables. 


\subsection{Vectors }

A vector is a (1D) list of numbers, for example, \lstinline![ 1 2 3]!.  The following example creates a variable called \lstinline!A! which is assigned the vector \lstinline![ 1 2 3 4 5 ]!.

\begin{lstlisting}[frame=single]
>> A = [ 1 2 3 4 5 ];
>> A
A =
     1     2     3     4     5
\end{lstlisting}


\noindent Note that if we add a semi-colon (`;'') to the end of the line, then MATLAB will not print out the result of the assignment.  To check that the variable we have created contains our vector, we can simply type \lstinline!A! (without the semi-colon). Alternately, we could look in the workspace window and select the variable  \lstinline!A! (assuming of course, that it has been defined!).

\bigskip

\noindent A vector is a handy object to store the domain (for example, the $x$ values) and range values (for example, the $y$ values) of a graph. Suppose we want to plot focal length against lens power. Furthermore, assume we are interested in plotting focal lengths for  lenses with powers ranging from -5D to 5D in steps of 1D (the $x$-values).  We can create and store all the required $x$-values in MATLAB using the following command.

\begin{lstlisting}[frame=single]
>> D = -5:1:5
D =
    -5  -4  -3  -2  -1   0   1   2   3   4   5
\end{lstlisting}

\noindent The range is specified by the first and last numbers, the step is given by the middle number. We would like to know the corresponding focal lengths for each of these powers (D).  The equation is $f = 1/D$.
We use the following code to perform the calculation:

\begin{lstlisting}[frame=single]
>> f = 1./D
f =
   -0.2000 -0.2500 -0.3333 -0.5000 -1.0000   Inf 1.0000  0.5000  0.3333  0.2500  0.2000

\end{lstlisting}

\noindent Each element of the returned vector \lstinline!f! corresponds to the element at the same position (or index) in the \lstinline!D! variable. The equation $f=1/D$ has been applied in an {\bf element-wise} fashion. In other words, the equation or operation works on each element in the named variable \lstinline!D!. The result is assigned to the corresponding element of \lstinline!f!.

\bigskip

\noindent {\bf Important note: } In order to let MATLAB know we would like it to work in an element-wise fashion we use the dot  operator (``.''). In this instance, we want to assign each element of the vector $f$ to the value of the corresponding element of $D$ after applying  the reciprocal $1/D$ to \emph{each} element 

\bigskip

\noindent It is interesting to try and visualize now the results of the calculation. We can plot $f$ vs $D$ using the following simple code:

\begin{lstlisting}[frame=single]
figure(1); clf; % <-- create a figure and clear it
plot(D, f); 	% <-- D is the x-axis, f is the y-axis 
xlabel('Lens power (D)'); % <-- add text to the x-axis of the graph  
ylabel('Focal length (m)'); % <-- add text to the y-axis of the graph
grid on;  % <-- add grids onto the graph
\end{lstlisting}

\noindent To see the results of this plot, type  in each command making sure to press enter after each line. 

\bigskip

\noindent The following additional commands add x and y axes to the graph:

\begin{lstlisting}[frame=single]
hold on;  % <-- hold the graph so extras can be added to it 
plot([-10 10],[0 0], 'k-'); % <-- plot a horizontal line for the x-axis 
plot([0 0],[-10 10], 'k-'); % <-- plot a vertical line for the y-axis 
axis([-5 5 -1 1]); % <-- make it so the graph goes from -5 to 5 
in the x direction, and -1 to 1 in the y-direction
\end{lstlisting}

\noindent The result is shown in  Figure \ref{fig:rough}. You may notice that the results are not ''smooth'', a situation we can improve by calculating the focal length over more data points in the given range.  the \lstinline!linspace! command will produce a requested number of equally spaced points ranging between two given limits.  For example, we can recalculate the \lstinline!D! and \lstinline!f! values using:

\begin{lstlisting}[frame=single]
D = linspace(-5,5,101);
f = 1./D;
\end{lstlisting}

\noindent After-which we repeat the plotting commands given above to re-show the graph.  In this case we could use updated axis values \lstinline![-5 5 -10 10]!   in order to ensure the graph fits nicely within the window.  The results are shown in Figure \ref{fig:smooth}.  Crucially we should note that \lstinline!D! has $101$ elements now rather than the original $11$.

\begin{figure}
\centering
\subfigure[``Rough'' example]{
   \includegraphics[scale=0.185]{./figures/figure-smooth-data.jpg}
%    \rule{4cm}{3cm}
    \label{fig:rough}
} 
\subfigure[`Smooth''example]{
   \includegraphics[scale=0.185]{./figures/figure-rough-data.jpg}
%    \rule{4cm}{3cm}
    \label{fig:smooth}
}
\caption{First plotting example}
\end{figure}


%
% \ begin{figure}
% \centering
%	\begin{subfigure}
%	\centering
%	\includegraphics[scale=0.25]{./figures/figure-smooth-data.jpg}
%	\caption{$D$ vs $f$ a ``rough'' version}
%	\label{fig:plot_a}
%	\end{subfigure}	
%	\begin{subfigure}
%	\centering
%	\includegraphics[scale=0.25]{./figures/figure-rough-data.jpg}
%	\caption{$D$ vs $f$ a ``smoother' version}
%	\label{fig:plot_b}
%	\end{subfigure}
% \end{figure}
%


\subsubsection{Vector operations}

\noindent As with scalars, vectors can be multiplied, divided, added or subtracted from each other. In this case we can define two further types of operations: (1) operations involving a scalar and vector, and (2) operations involving two vectors.  In the former case the operations work in an { \bf element-wise } fashion. That means that the operation is carried out on each element of the vector with the same scalar.  Hence the following results,

\begin{lstlisting}[frame=single]

>> I = [ 1 2 3 4 5 6 ];
>> I + 5
ans =
     6     7     8     9    10    11
>> 3*I
ans =
     3     6     9    12    15    18
>> I/2
ans =
    0.5000    1.0000    1.5000    2.0000    2.5000    3.0000
\end{lstlisting}

\noindent The vector versions of the elementary operations are slightly different.  Whilst addition/subtraction of vectors is element-wise, mulitplication and division need not necessarily be. Suppose we have another vector, called \lstinline!K!.  Then we can perform element-wise operations using the code as follows:

\begin{lstlisting}[frame=single]
>> K = [ 2 0 5 6 7 8 ];
>> I + K
ans =
     3     2     8    10    12    14
>> I - K
ans =
    -1     2    -2    -2    -2    -2
>> I.*K
ans =
     2     0    15    24    35    48
>> I./K
ans =
    0.5000       Inf    0.6000    0.6667    0.7143    0.7500
>> 
\end{lstlisting}

\noindent Notice that multiplication (``*'') and division (``/'') have the dot operator  (`.'') in-front of it.  This tells MATLAB that the multiplication/division will be element-wise.  If we forget the dot operator we will get strange results, for example:

\begin{lstlisting}[frame=single]
>> I*K
Error using  * 
Inner matrix dimensions must agree.
>> I/K
ans =
    0.6966
\end{lstlisting}

\noindent Without the dot operator, MATLAB will attempt to do another type of multiplication/division. We will not concern ourselves further with this type of multiplication/division.

\subsection{Matrices}

Matrices extend upon the idea of vectors, by adding an extra dimension(s).  For example, a grayscale image can be thought of as a 2D object (a grid of numbers), whilst a color (RGB) image is thought of as a 3D object (a cube of numbers).  Matrices of higher dimensions are possible (for example, an RGB video is 4-dimensional).

\bigskip 

\noindent We can declare a matrix using the following form,

\begin{lstlisting}[frame=single]
>> I =[ 1 2 3 ; 4 5  6 ]
I =
     1    2   3
     4    5   6
 >> \end{lstlisting}

\noindent This matrix has two rows and three columns as you can verify the size by using the size command.


\begin{lstlisting}[frame=single]
>> size(I)
ans =
     2     3
 >> \end{lstlisting}

\noindent As with vectors and scalars we can also perform addition, subtraction, multiplication and division by scalars as well as other matrices.  Firstly experiment with scalar multiplication, for example,

\begin{lstlisting}[frame=single]
>> -2.3*I
ans =
   -2.3000   -4.6000   -6.9000
   -9.2000  -11.5000  -13.8000
\end{lstlisting}

\noindent and then scalar addition, for example:

\begin{lstlisting}[frame=single]
>> I + 5
ans =
     6     7     8
     9    10    11
\end{lstlisting}

\noindent These operations are again {\bf element-wise operations}. Create a new $2 \times 3$ matrix then check that addition and subtraction are also element-wise operations.  Also note that the dot operator is needed before the multiplication/division sign in order to perform element-wise multiplication/division.  Finally, note that we cannot perform element-wise operations when two matrices are of differing sizes.

\subsubsection{Indexing} 

We have covered some of this in lectures, and it you may wish to confirm the results shown in those lectures.
The elements of a matrix may be referred to/or set by naming the row(s) and column(s) we are interested in.
We declare a matrix below: 

\begin{lstlisting}[frame=single]
>> A = [ 1 2 3 4 ; 5 6 7 8 ; 10 20 30 40 ; 50 60 70 80 ];
>> A
A =
     1     2     3     4
     5     6     7     8
    10    20    30    40
    50    60    70    80
\end{lstlisting}

 \noindent Assign 30 to the $3^{rd}$ row,

\begin{lstlisting}[frame=single]
>> A(3,:)=1
A =
     1     2     3     4
     5     6     7     8
     1     1     1     1
    50    60    70    80
\end{lstlisting}

\noindent and the first column,

\begin{lstlisting}[frame=single]
>> A(:,1)=1
A =
     1     2     3     4
     1     6     7     8
     1     1     1     1
     1    60    70    80
\end{lstlisting}


\noindent You may also get values from a matrix by naming the rows and columns appropriately,

\begin{lstlisting}[frame=single]
>> A(1:2,1:2)
ans =
     1     2
     1     6
\end{lstlisting}

\subsection{Loading/Saving variables}

It is often the case that we will wish to save or load our work as well as the work of others into the workspace.  
The following example creates two matrices and then saves the two associated  variables as \lstinline!example.mat!.  We then use the \lstinline!clear all! command to wipe the workspace, and then load back the variables from disk to restore them.

\begin{lstlisting}[frame=single]
>> A =rand(5);
>> B = rand(5);
>> save('example.mat','A','B');
>> clear all
>> load('example.mat');
\end{lstlisting}

\noindent After performing these commands the workspace will show that \lstinline!A! and \lstinline!B! are in the workspace, and we can use them as previously. This code demonstrates that the variables can be saved, cleared and re-loaded.  

\subsection{Creating an image in MATLAB}

\noindent We can create an image using basic matrix operations becuase an image is simply a matrix.
 
 \begin{lstlisting}[frame=single]
I = zeros(101); % image of 101 * 101 zeros
I(:,1)=255;  I(:,101)=255;  % two commands per line 
I(1,:)=255; I(101,:)=255;   % two commands per line
I(50,:) = 255; I(:,50)=255; % two commands per line
\end{lstlisting}

\bigskip

\noindent What could this be an image of?  Use \lstinline!imshow! to display the result.

\subsection{Loading/saving images using MATLAB}

MATLAB provides simple functions for loading (and saving) images (as well as other types of data) from and to major image formats.  If we have an image on the disk then we can use the \lstinline!imread! command to read the image into a specified variable given the name of the image file. The following example loads  \lstinline!orange.jpg! into the workspace as the variable \lstinline!I!.  We check its dimensions using the \lstinline!size! command, and then display it using  \lstinline!imshow!.The result is shown in Figure \ref{fig:orange}.

\begin{lstlisting}[frame=single]
I = imread('orange.jpg');
>> size(I)
ans =
   201   251     3
>>imshow(I);
\end{lstlisting}

\begin{figure}
\centering
\includegraphics[scale=0.3]{./figures/figure-orange.jpg}
\caption{\lstinline!orange.jpg!}
\label{fig:orange}
\end{figure}

\noindent A particular matrix can be saved as an image file using the  \lstinline!imwrite! command but we will not need to use it.  Another way to save a file is to use the menu options in the figure window to export the image to a file.  {\bf You may find this useful for generating reports for example }  An example figure window is shown in Figure \ref{fig:figure_window}.

\begin{figure}
\centering
   \includegraphics[scale=0.4]{./figures/matlab-figure-window.png}
  \caption{Contents of figure window can be saved.}
   \label{fig:figure_window}
\end{figure}


\bigskip 

\noindent There a few ways to show an image file, for example:  \lstinline!imshow!,  \lstinline!imagesc! or  \lstinline!image! .  Each method has slightly different approach to showing the information contained in the matrix given to it.  Typically we would use \lstinline!imshow!.  

\pagebreak

\subsection{Plotting}

We have used the plot function already in this tutorial.  There are many possible options, too many to include here. 
However, here are two simple examples to assist you in putting together your own possible plots. 

\begin{lstlisting}[frame=single]
i = 1:100; % x -axis
 y = sin(2*pi/50*i); % y-axis
figure, clf;
 plot(i,y);
grid on;
xlabel('x');
ylabel('y');
 title('y=sin(x)');
\end{lstlisting}

\noindent We can show multiple trigonometric curves on the same graph.

\begin{lstlisting}[frame=single]
r = sin(2*pi/50*i);
g = sin(2*pi/25*i);
b = sin(2*pi/10*i);
plot(i,r,'r-', i,g, 'g-', i,b,'b-');
grid on;
xlabel('x');
ylabel('y');
title('multiple graphs');
legend('r','g','b');
\end{lstlisting}


\noindent The results for the two examples are shown in Figures \ref{fig:single_curve} and \ref{fig:multi_curve}.

%\begin{figure}
 %\centering
%	\begin{subfigure}
%	\centering
%	\includegraphics[scale=0.25]{./figures/figure-single-curves.jpg}
%	\caption{$D$ vs $f$ a ``rough'' version}
%	\label{fig:plot_aa}
%	\end{subfigure}	
%	\begin{subfigure}
%	\centering
%	\includegraphics[scale=0.25]{./figures/figure-multi-curves.jpg}
%	\caption{$D$ vs $f$ a ``smoother' version}
%	\label{fig:plot_bb}
%	\end{subfigure}
%\end{figure}

\begin{figure}
\centering
\subfigure[Single curve plot]{
   \includegraphics[scale=0.185]{./figures/figure-single-curves.jpg}
%    \rule{4cm}{3cm}
    \label{fig:single_curve}
} 
\subfigure[Multiple curve plot]{
   \includegraphics[scale=0.185]{./figures/figure-multi-curves.jpg}
%    \rule{4cm}{3cm}
    \label{fig:multi_curve}
}
\caption{Plotting examples}
\end{figure}

\subsection{Performing convolution}

We can perform convolution using the \lstinline!conv2! function.  For example try the following code snippet

\begin{lstlisting}[frame=single]
I = imread('lena.bmp' );K = [ 1 0 0 ; 0 -1 0 ; 0 0 1 ] ;I2 = conv2 (double(rgb2gray(I)) ,K, 'same' ) ;
figure(1); clf;
subplot(1,2,1);
imshow(I); title('Before imfilter');
subplot(1,2,2);
imshow(mat2gray(I2));  title('After imfilter');
\end{lstlisting}

\noindent This basic code snippet illustrates a number of things needed to make the convolution work. For example, notice that the image has been converted into a grayscale image (see \lstinline!rgb2gray!), then converted into a double (see \lstinline!double!).  The \lstinline!same! option in the convolution function keeps the resulting image the same size as the input image. Also notice that when the image is displayed it is further converted into an image with a value between 0 and 1 (see \lstinline!mat2gray!).  The final image is actually a grayscale image so we lose  the color channels in this particular code snippet.

Fortunately MATLAB provides a much more convenient function that we can use for image filtering. it is called the \lstinline!imfilter! function. The following code snippet loads the image \lstinline!lena.bmp! and then performs convolution with the same simple kernel (that we call K).

\begin{lstlisting}[frame=single]
I = imread('lena.bmp');
K = [ 1 0 0 ; 0 -1 0 ; 0 0 1 ];
I2 = imfilter(I,K);
figure(1); clf;
subplot(1,2,1);
imshow(I); title('Before imfilter');
subplot(1,2,2);
imshow(I2); title('After imfilter');
\end{lstlisting}

\noindent Finally we note that we can generate kernels by using the  \lstinline!fspecial! command.  The following command creates a disk kernel with radius of 3 pixels.  This is then convolved with the original image using \lstinline!imfilter!
 
\begin{lstlisting}[frame=single]
I = imread('lena.bmp');
K = fspecial('disk',3);
I2 = imfilter(I,K);
figure(1); clf;
subplot(1,2,1);
imshow(I); title('Before imfilter');
subplot(1,2,2);
imshow(I2); title('After imfilter');
\end{lstlisting}
 
 
%\begin{figure}
%\centering
%\subfigure[Lena using \lstlisting!imshow!]{
%  \includegraphics[scale=0.185]{./figures/lena-convolved-imshow.jpg}
%    \label{fig:lena_imshow}} 
%\subfigure[Lena using \lstlisting!imagesc!]{
%  \includegraphics[scale=0.185]{./figures/lena-convolved-imagesc.jpg}
%    \label{fig:lena_imagesc}}
%\end{figure}

\

\nocite{wallisch2010matlab}
\nocite{Doe:2009:Online}
\bibliographystyle{plain}
\bibliography{bibliography}



\end{document}
