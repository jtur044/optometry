

disp('Hello World.'); %<? press enter

pause;


help

pause;


help images;

pause;


4+6
7-3
2+3
10/2
pause;


2+3+4-7+5+8/9+1-5*6/3
2+3+4-7*(5+8/9)+1-5*6/3

pause;

y = 5;
x = 12;
pause;


A = [ 1 2 3 4 5 ];
A

pause;

D = -5:1:5;
pause;

f = 1./D
pause;


figure (1); clf ; % <?? create a figure and clear it
plot(D, f );	% <?? D is the x?axis , f is the y?axis
xlabel('Lens power (D)'); % <?? add text to the x?axis of the graph
ylabel('Focal length (m)'); % <?? add text to the y?axis of the graph
grid on;
pause;

hold on; %<?? hold the graph so extras can be added to it 
plot([-10 10] ,[0 0],'k-' ); % <?? plot a horizontal line for the x?axis 
plot([0 0],[-10 10], 'k-'); %<?? plot a vertical line for the y?axis 
axis([-5 5 -1 1]); %<?? make it so the graph goes from ?5 to 5 in the x direction , and ?1 to 1 in the y?direction
pause;


D = linspace ( -5 ,5 ,101); f = 1./D;
figure (1); clf ; % <?? create a figure and clear it
plot(D, f );	% <?? D is the x?axis , f is the y?axis
xlabel('Lens power (D)'); % <?? add text to the x?axis of the graph
ylabel('Focal length (m)'); % <?? add text to the y?axis of the graph
grid on;
hold on; %<?? hold the graph so extras can be added to it 
plot([-10 10] ,[0 0],'k-' ); % <?? plot a horizontal line for the x?axis 
plot([0 0],[-10 10], 'k-'); %<?? plot a vertical line for the y?axis 
axis([-5 5 -1 1]); %<?? make it so the graph goes from ?5 to 5 in the x direction , and ?1 to 1 in the y?direction
pause;


I = [ 1 2 3 4 5 6 ];
I + 5
3*I
I/2
pause;

K = [ 2 0 5 6 7 8 ];
I + K
I - K
I.*K
I./K
pause;


I*K
I/K
pause;


I=[1 2 3;4 5 6]
pause;

size(I)
pause;


-2.3*I
pause;

I+5
pause;

A = [ 1 2 3 4 ; 5 6 7 8; 10 20 30 40 ; 50 60 70 80 ] 
pause;

A(3,:) = 1
pause;

A(:,1) = 1
pause;

A(1:2,1:2)
pause;


A =rand(5); 
B = rand(5); 
save('example.mat','A','B');
clear all
load('example.mat');
pause;


I = imread('orange.jpeg');
size(I)
imshow(I);
pause;

i = 1:100; % x ?axis 
y = sin(2*pi/50*i); % y?axis
figure, clf;
plot(i,y);
grid on;
xlabel('x');
ylabel('y');
title('y=sin (x)');
pause;


% Show multiple trigonometric curves on the same graph.
r = sin(2*pi/50*i); 
g = sin(2*pi/25*i); 
b = sin(2*pi/10*i); 
plot(i,r,'r-', i,g, 'g-', i,b,'b-'); 
grid on ;
xlabel('x'); 
ylabel('y'); 
title('multiple graphs'); 
legend ('r','g','b');
pause;

I = imread('lena.bmp'); 
K = [ 1 0 0 ; 0 -1 0 ; 0 0 1 ] ;
I2 = conv2(I,K);
pause;

class(K)
pause;


I2 = conv2(double(rgb2gray(I)),K,'same');
pause;

%I = imread('lena.bmp');
%K = [ 1 0 0 ; 0 -1 0 ; 0 0 1 ];
%I2 = conv2(double(rgb2gray(I)),K,'same');
%figure(1); clf; imshow(I2);
%figure(2); clf; imagesc(I2);
%pause;


I2 = conv2(double(rgb2gray(I)),K,'same');
figure(1); clf; imshow(mat2gray(I2));
pause;








