function I2 = convolution(I, K, stroption)

% CONVOLUTION 2D convolution for 
%
%   I2 = convolution(I, K, [option])
%
% where 
%       I is the image 
%       K is the kernel
%       option is optional and can be 'same' or 'valid'
%
% see the documentation for CONV2 for the meaning of 'same' and 'valid' 
%

if (nargin == 2)
    stroption = 'same';
end  
 
I2 = conv2 (double(rgb2gray(I)),K, stroption);
I2 = mat2gray(I2);
return