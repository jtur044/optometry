function z = mexihat(n)
p = linspace(-8,8,n);
[x,y] = meshgrid(p);
r = sqrt(x.^2+y.^2)+eps;
z = sin(r)./r;
end
