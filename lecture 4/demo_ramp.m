%% DEMO_RAMP Example showing results of Mach band Illusion
%
%

I = imread('ramp.png');
I = mat2gray(I);
K = csvread('mexicanhat_mach.txt');
R = imfilter(I, K); 
R = mat2gray(R);
figure(2); clf; imshow(R);
title('Convolution example');