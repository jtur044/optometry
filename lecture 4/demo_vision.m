%% DEMO_VISION Demonstrate different convolution methods 
%
% Examples based on LECTURE 4 
% 

close all;

I = imread('grid.png');
I = rgb2gray(I);
I = mat2gray(I);

%% Original grid + results
figure(1);clf;
imshow(I);

figure(2);clf;
K = mexihat(41);
%K = csvread('mexicanhat.csv');
R = imfilter(I, K, 'same');
imshow(mat2gray(R));
title('Show convolved result');

title('Original grid');
surf(mat2gray(R)); shading flat;

%%%%% Mach bands illusion 
%
%
%
%

I = imread('ramp.png');
I = rgb2gray(I);
I = mat2gray(I);

figure(3);clf;
imshow(I);
title('Original grid');

figure(4);clf;
K = csvread('mexicanhat_mach.txt');
R = imfilter(I, K, 'replicate');
R = mat2gray(R);  % 0 -> 1
imshow(R);
title('Show convolved result');

%figure(5);
%I = imread('ramp.png');
%K = csvread('mexicanhat_mach.txt');
%I2 = conv2(double(rgb2gray(I)),K,'same');
%figure(5); clf; imshow(mat2gray(I2));
%pause;

return