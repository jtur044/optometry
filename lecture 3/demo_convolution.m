%% DEMO_CONVOLUTION Demonstrate different convolution methods 
%
% Examples based on LECTURE 3 Lecture 3
% 

close all;

%% Demonstrate spherical blur
I = imread('Eye_disease_simulation,_normal_vision.jpg');
I = imresize(I, 0.2);
K = fspecial('disk', 5);  % <-- a circle disk kernel
R = imfilter(I, K, 'same');

% show Kernel 
K

% show the original
figure(1); clf;
imshow(I);
%title('Original');

% Show the filtered 
figure(2); clf;
imshow(R);
%title('Spherical Blur');
    


%% Demonstrate motion blur
I = imread('bloom-blossom-close-up-46196.jpg');
I = imresize(I, 0.1); % <--- make it smaller !
K = eye(15)/15; % <-- produces a simple motion blur kernel (could use fspecial as well!)
R = imfilter(I, K, 'same');

% show the original
figure(1); clf;
imshow(I);
%title('Original');

% Show the filtered 
figure(2); clf;
imshow(R);
%title('Motion Blur');
    

%% Demonstrate edge detection 
I = imread('flowers.jpg');
I = imresize(I, 0.3);

% made a custom kernel here
K = [   -1, -1, -1 ; ...
        -1,  8, -1 ; ...
        -1, -1, -1 ];
    
R = imfilter(I, K, 'same');


% show the original
figure(3); clf;
imshow(I);
%title('Original');

% Show the filtered 
figure(4); clf;
imshow(R);
%title('Edge Detection');


%% Demonstrate some basic 1D example using MATLAB
%
% 
%

% create image filtering example 
I = [ 2 2 2 2 2 4 6 6 6 6 6 ];
result = conv(I,[-1,+2,1],'same');



%% Demonstrate image enahncement with different kernels
%
% 
%

I = imread('Lena.png');

%%
% demonstrate the delta function
K = [ 0 0 0 ; ...
      0 1 0 ; ...
      0 0 0 ];

R = imfilter(I, K);  
figure(5); clf;
subplot(1,2,1);
imshow(I);
subplot(1,2,2);
imshow(R);

%%
% demonstrate spherical blur 

K = fspecial('disk', 16);  
R = imfilter(I, K);  

figure(6); clf;
subplot(1,2,1);
imshow(I);
subplot(1,2,2);
imshow(R);

%%  
% demonstrate astigmatic blur 

 K = fspecial('motion', 20, 45);
 R = imfilter(I, K);  

figure(7); clf;
subplot(1,2,1);
imshow(I);
subplot(1,2,2);
imshow(R);  

%%
% demonstrate horizontal edge finder  

 K = [ 0 0 -1 0 0  ; ... 
       0 0 -1 0 0  ; ...
       0 0  4 0 0  ; ... 
       0 0 -1 0 0  ; ...
       0 0 -1 0 0  ];
   
R = imfilter(I, K);  
figure(8); clf;
%subplot(1,2,1);
%imshow(I);
%subplot(1,2,2);
imshow(R);  



%%
% demonstrate vertical edge finder  
K = [ 0 0 0 0 0  ; ... 
       0 0 0 0 0  ; ...
       -1 -1  4 -1 -1  ; ... 
       0 0 0 0 0  ; ...
       0 0 0 0 0  ];
   
R = imfilter(I, K);  
 
figure(9); clf;
%subplot(1,2,1);
%imshow(I);
%subplot(1,2,2);
imshow(R);  

%% Demonstrate response on a grid 
%
% 
%

figure(10); clf;
I = imread('grid.png');


% first lets put the image in the range [0 -> 1]
I = rgb2gray(I);
I = mat2gray(I);

% vertical filter s
K = [  -1 ; ...
        2 ; ...
       -1 ];
   
% first lets put the image in the range [0 -> 1]
R = conv2(I, K, 'valid');
imshow(mat2gray(R));









