%% Demonstrate some basic matlab 
%
% These are some examples adapted from notes LEC 2
%

%% EXAMPLE 1 - Get/set pixels 
% This example will demonstrate getting and setting:
% Roughly based on LEC 2 notes 
%


%% Create a matrix I 

I = [   52 55 61 66 70 61 64 73 ; ...
        63 59 55 90 109 85 69 72 ; ...
        62 59 68 113 144 104 66 73 ; ...
        63 58 71 122 154 106 70 69 ; ...
        67 61 68 104 126 88 68 70 ; ...
        79 65 60 70 77 68 58 75 ; ...
        85 71 64 59 55 61 65 83 ; ...
        87 79 69 68 65 76 78 94 ];

%% Show the matrix as numeric values   
I 

%% Get pixels explicitly  
I(2,8)
I(3:5,8)

%% Get pixels implicitly   
I(3,:) 
I(:,4) 

%% Set pixels implicitly 
I(3,:) = [ 10 11 12 13 144 104 60 12 ]
I(:,5) = [ 60 70 83 22 10 10 12 10 ]' % <- notice the ' makes a column vector

%% What would be the value of I(3,5) ?
I(3,5) 


%% EXAMPLE 2 - Manipulating a color image
%
% This exmaple will demonstrate:
%  1 - Load an image 
%  2 - Show the image in Figure 1 
%  3 - Show each channel in Figure 2 
%  4 - Create a 5x5 image called J
%  5 - Get/set on J
%


%% Load an image 
I = imread('Lena.png');  % load Lena.png into the variable I 
size(I) % What is the size of 'Lena.png' (nb: no ; suppress output)

R = I(:,:,1); % <- Red channel
G = I(:,:,2); % <- Green channel
B = I(:,:,3); % <- Blue channel

%% Show an image I 
figure(1); clf;
imshow(I);

%% Show each color channel, image (R,G,B)
figure(2); clf;
subplot(1,3,1);
imshow(R);
title('Red channel');

subplot(1,3,2);
imshow(G);
title('Green channel');

subplot(1,3,3);
imshow(B);
title('Blue channel');


%% Crop the image to something smaller (e.g., 5 x 5)
J = I(1:5,1:5,:);  % <- get a sub 5x5x3 image 

%% Getting + setting
J(5,1,:)
J(5,5,:) = [ 82 10 11 ]


%% EXAMPLE 3 - image manipulation (orange + apple)
%
%  1 - scaling 
%  2 - scalar add  
%  3 - image add subtract  

% read the image !
original = imread('apple-orange.jpg');

% show the original apple
figure(3); clf;
imshow(I);
title('Apple and orange together.');

% define the region in which orange and apple reside 
OrangeCropRegion = [ 42  23 877 928 ]; % col, row, width, height 
AppleCropRegion  = [ 920 23 877 928 ]; % col, row, width, height 

% use 'imcrop' function to get the image 
% (we could have gotten the image )
%
%
AppleImage  = imcrop(original, AppleCropRegion); % this will get the i
OrangeImage = imcrop(original, OrangeCropRegion); % this will get the i



figure(4); clf;
subplot(1,2,1);
imshow(AppleImage);
title('Original apple image');

subplot(1,2,2);
imshow(OrangeImage);
title('Original orange image');

%% Perform scaling and scalar addition
%  - scale the orange by a mulplier (say 2) 
%  - add 10 to the apple 

OrangeScaled = 2*OrangeImage;
AppleAdded   = AppleImage + 10;

figure(5); clf;
subplot(2,2,1);
imshow(OrangeScaled);
title('A scaled orange');

subplot(2,2,2);
imshow(AppleAdded);
title('Scalar addition to apple');

AverageImage = (0.66*OrangeImage + 0.33*AppleImage);
subplot(2,2,3);
title('Average of apple to the orange');
imshow(AverageImage);
title('Mostly orange.');


AverageImage = (0.33*OrangeImage + 0.66*AppleImage);
subplot(2,2,4);
title('Average of apple to the orange');
imshow(AverageImage);
title('Mostly apple.');
