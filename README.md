# Optometry example codes 

Welcome! Here you will find various codes for demonstrating physiological optics and image processing.

PLEASE MAKE SURE YOU READ THE INFORMATION BELOW! :)

## Getting Started

In order to make use of the codes on this page youll need to have Matlab (or alternately Octave) (see Prerequisites below for information on obtaining it).  Youll also need the image processing toolbox for Matlab, which is an add-on to the basic matlab system that adds some useful commands/functions for manipulating images.

In order to get the codes on this page, navigate to the Downloads section in order to download all the codes and images as a single ZIP file. Once it has downloaded, you should copy the file to a convenient location (for example, the Desktop of your computer), and then unzip it. This should create a directory called *something like* "optometry" on the Desktop (if you decided to place it there).

Open MATLAB and then navigate (using Matlab) to the directory where the files are stored. Matlab programs end with the .m extension. You should be able to see  for example: demo_basic_matlab.m. By typing this command (i.e., type: demo_basic_matlab and hit enter) at the command-line you should be able to run it. If you do this, you will see that it creates a  bunch of figures and output. cool!

It is possible to record the results of a program while it runs. This is handy because the output will be saved and it will be saved in a convenient form. We do this by calling the "publish" command along with the name of the program you want to run. For example, enter "publish demo_basic_matlab" at the command line and hit enter. By default, the results of this program will be summarized in the "html" directory.  You should see a file called "demo_basic_matlab.html" in the "html" directory". You can open by double-clicking on it, or by right clicking on it and choosing "open outside of Matlab". The output of the program should show as a nicely formatted web page.

The demo_basic_matlab program runs through some very basic use cases of Matlab related to image manipulation.
Have fun!

### Prerequisites

University of Auckland students are entitled to use Matlab without charge:
https://www.software.auckland.ac.nz/en/matlab.html

Matlab comes with various toolboxes (see Products available). The installation will ask you to install toolboxes of your choice. In order to use examples in here you will likely need the "Image Processing Toolbox".

If you are not familiar at all with Matlab then you may want to look up:  https://au.mathworks.com/help/matlab/getting-started-with-matlab.html

Octave can be accessed here:
https://www.gnu.org/software/octave/

## Authors

* **Jason Turuwhenua** - *Initial work* - 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
